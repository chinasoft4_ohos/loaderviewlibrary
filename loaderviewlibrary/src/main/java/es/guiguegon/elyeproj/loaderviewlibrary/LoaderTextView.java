package es.guiguegon.elyeproj.loaderviewlibrary;

/*
 * Copyright 2016 Elye Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.app.dispatcher.TaskDispatcher;

public class LoaderTextView extends Text implements LoaderView, Component.DrawTask, Component.BindStateChangedListener {

    private LoaderController loaderController;
    private Color defaultColorResource;
    private Color darkerColorResource;
    private Context mContext;

    public LoaderTextView(Context context) {
        super(context);
        this.mContext = context;
    }

    public LoaderTextView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        this.mContext = context;
        init(attrSet);
        loaderController.onSizeChanged();
    }

    public LoaderTextView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        this.mContext = context;
        init(attrSet);
        loaderController.onSizeChanged();
    }

    private void init(AttrSet attrs) {
        defaultColorResource = AttrValue.get(attrs, "custom_color", new Color(ResourceTable.Color_default_color));
        darkerColorResource = AttrValue.get(attrs, "custom_color", new Color(ResourceTable.Color_darker_color));
        loaderController = new LoaderController(this,mContext);
        loaderController.setWidthWeight(AttrValue.get(attrs, "width_weight", LoaderConstant.MAX_WEIGHT));
        loaderController.setHeightWeight(AttrValue.get(attrs, "height_weight", LoaderConstant.MAX_WEIGHT));
        loaderController.setUseGradient(AttrValue.get(attrs, "use_gradient", LoaderConstant.USE_GRADIENT_DEFAULT));
        loaderController.setCorners(AttrValue.get(attrs, "corners", LoaderConstant.CORNER_DEFAULT));
    }

    public void resetLoader() {
        if (!AttrValue.isEmpty(getText())) {
            super.setText(null);
            loaderController.startLoading();
        }
    }

    @Override
    public void setText(String text) {
        super.setText(text);
        if (loaderController != null) {
            loaderController.stopLoading();
        }
    }

    @Override
    public void setRectColor(Paint rectPaint) {
        if (getFont() != null && getFont().getWeight() == Font.BOLD) {
            rectPaint.setColor(darkerColorResource);
        } else {
            rectPaint.setColor(defaultColorResource);
        }
    }

    @Override
    public void invalidate() {
        super.invalidate();
        addDrawTask(this::onDraw);
    }

    @Override
    public boolean valueSet() {
        return !AttrValue.isEmpty(getText());
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {

        loaderController.setWidth(getWidth());
        loaderController.setHeight(getHeight());
        Element leftElement = getLeftElement();
        Element topElement = getTopElement();
        Element rightElement = getRightElement();
        Element botoomElement = getBottonElement();

        int leftPadding = getPaddingLeft();
        int topPadding = getPaddingTop();
        int rightPadding = getPaddingRight();
        int bottomPadding = getPaddingBottom();

        if(leftElement != null){
            leftPadding += leftElement.getWidth();
        }

        if(topElement != null){
            topPadding += topElement.getHeight();
        }

        if(rightElement != null){
            rightPadding += rightElement.getWidth();
        }

        if(botoomElement != null){
            bottomPadding += botoomElement.getHeight();
        }

        loaderController.onDraw(canvas, leftPadding,
                topPadding,
                rightPadding,
                bottomPadding);
    }

    @Override
    public void onComponentBoundToWindow(Component component) {

    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        loaderController.removeAnimatorUpdateListener();
    }
}
