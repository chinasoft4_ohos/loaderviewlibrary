/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package es.guiguegon.elyeproj.loaderviewlibrary;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * AttrValue
 *
 * @ProjectName: loadrviewlibrary
 * @ClassName: AttrValue
 * @Description:
 * @Author: lr
 * @CreateDate: 2021/5/28 10:27
 * @since 2021-05-28
 */
public class AttrValue {
    private static HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, 0x001, "AttrValue");
    private static final int CONSTANT = 160;
    private AttrValue() {
    }

    /**
     * getattrset
     *
     * @param attrSet AttrSet
     * @param key String
     * @param defValue T
     * @param <T> T
     * @return T
     */
    public static <T> T get(AttrSet attrSet, String key, T defValue) {
        if (attrSet == null || !attrSet.getAttr(key).isPresent()) {
            return (T) defValue;
        }
        Attr attr = attrSet.getAttr(key).get();
        if (defValue instanceof String) {
            return (T) attr.getStringValue();
        } else if (defValue instanceof Long) {
            return (T) (Long) (attr.getLongValue());
        } else if (defValue instanceof Float) {
            return (T) (Float) (attr.getFloatValue());
        } else if (defValue instanceof Integer) {
            return (T) (Integer) (attr.getIntegerValue());
        } else if (defValue instanceof Boolean) {
            return (T) (Boolean) (attr.getBoolValue());
        } else if (defValue instanceof Color) {
            return (T) (attr.getColorValue());
        } else if (defValue instanceof Element) {
            return (T) (attr.getElement());
        } else {
            return (T) defValue;
        }
    }

    /**
     * getLayout
     *
     * @param attrSet AttrSet
     * @param key String
     * @param def int
     * @return int
     */
    public static int getLayout(AttrSet attrSet, String key, int def) {
        if (attrSet.getAttr(key).isPresent()) {
            int layoutId = def;
            String value = attrSet.getAttr(key).get().getStringValue();
            if (value != null) {
                String subLayoutId = value.substring(value.indexOf(":"));
                HiLog.info(logLabel, "getLayout", subLayoutId);
                layoutId = Integer.parseInt(subLayoutId);
            }
            return layoutId;
        }
        return def;
    }

    /**
     * getDimension
     *
     * @param attrSet AttrSet
     * @param key String
     * @param defDimensionValue int
     * @return int
     */
    public static int getDimension(AttrSet attrSet, String key, int defDimensionValue) {
        if (!attrSet.getAttr(key).isPresent()) {
            return defDimensionValue;
        }
        Attr attr = attrSet.getAttr(key).get();
        return attr.getDimensionValue();
    }

    /**
     * isEmpty
     *
     * @param str String
     * @return boolean
     * */
    public static boolean isEmpty(String str) {
        boolean isEmpty;
        if (str == null || "".equals(str)) {
            isEmpty = true;
        } else {
            isEmpty = false;
        }
        return isEmpty;
    }

    /**
     * px转vp
     *
     * @param context 上下文
     * @param px px
     * @return vp
     */
    public static int pxTovp(Context context, float px) {
        return Math.round(px) / (context.getResourceManager().getDeviceCapability().screenDensity / CONSTANT);
    }

}
