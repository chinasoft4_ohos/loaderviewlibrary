package es.guiguegon.elyeproj.loaderviewlibrary;

/*
 * Copyright 2016 Elye Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

public class LoaderImageView extends Image implements LoaderView, Component.DrawTask, Component.BindStateChangedListener {

    private LoaderController loaderController;
    private Color defaultColorResource;
    private Context mContext;

    public LoaderImageView(Context context) {
        super(context);
        this.mContext = context;
    }

    public LoaderImageView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        this.mContext = context;
        init(attrSet);
        loaderController.onSizeChanged();
    }

    public LoaderImageView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        this.mContext = context;
        init(attrSet);
        loaderController.onSizeChanged();
    }

    private void init(AttrSet attrs) {
        defaultColorResource = AttrValue.get(attrs, "custom_color", new Color(ResourceTable.Color_default_color));
        loaderController = new LoaderController(this,mContext);
        loaderController.setUseGradient(AttrValue.get(attrs, "use_gradient", LoaderConstant.USE_GRADIENT_DEFAULT));
        loaderController.setCorners(AttrValue.get(attrs, "corners", LoaderConstant.CORNER_DEFAULT));
    }

    public void resetLoader() {
        if (getImageElement() != null) {
            this.setPixelMap(null);
            loaderController.startLoading();
        }
    }

    @Override
    public void setRectColor(Paint rectPaint) {
        rectPaint.setColor(defaultColorResource);
    }

    @Override
    public boolean valueSet() {
        return (getImageElement() != null);
    }

    @Override
    public void setPixelMap(PixelMap pixelMap) {
        super.setPixelMap(pixelMap);
        if (loaderController != null) {
            loaderController.stopLoading();
        }
    }

    @Override
    public void setImageElement(Element element) {
        super.setImageElement(element);
        if (loaderController != null) {
            loaderController.stopLoading();
        }
    }

    @Override
    public void setPixelMap(int resId) {
        super.setPixelMap(resId);
        if (loaderController != null) {
            loaderController.stopLoading();
        }
    }

    @Override
    public void invalidate() {
        super.invalidate();
        addDrawTask(this::onDraw);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        loaderController.setWidth(getWidth());
        loaderController.setHeight(getHeight());
        loaderController.onDraw(canvas);
    }

    @Override
    public void onComponentBoundToWindow(Component component) {

    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        loaderController.removeAnimatorUpdateListener();
    }
}
