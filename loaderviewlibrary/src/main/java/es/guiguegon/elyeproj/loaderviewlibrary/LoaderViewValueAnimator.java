/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package es.guiguegon.elyeproj.loaderviewlibrary;

import ohos.agp.animation.AnimatorValue;

/**
 * SinValueAnimator
 *
 * @ProjectName: loadrviewlibrary
 * @ClassName: SinValueAnimator
 * @Description:
 * @Author: lr
 * @CreateDate: 2021/5/28 10:27
 * @since 2021-05-28
 */
public class LoaderViewValueAnimator extends AnimatorValue {
    private float start = 0;
    private float end = 1;
    private float values = 0.0f;
    private ValueUpdateListener myValueUpdateListener;

    private LoaderViewValueAnimator() {
        super.setValueUpdateListener(new ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                values = value * (end - start) + start;
                if (myValueUpdateListener != null) {
                    myValueUpdateListener.onUpdate(animatorValue, values);
                }
            }
        });
    }
    /**
     * 获取一个自定义初始值和结束值的数值动画对象
     *
     * @param start 起始值
     * @param end   结束值
     * @return 自定义初始值和结束值的数值动画对象
     */
    public static LoaderViewValueAnimator ofFloat(float start, float end) {
        LoaderViewValueAnimator myValueAnimator = new LoaderViewValueAnimator();
        myValueAnimator.start = start;
        myValueAnimator.end = end;
        return myValueAnimator;
    }

    @Override
    public void setValueUpdateListener(ValueUpdateListener listener) {
        this.myValueUpdateListener = listener;
    }
}
