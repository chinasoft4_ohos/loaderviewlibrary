package es.guiguegon.elyeproj.sampleloaderview;

import ohos.aafwk.ability.AbilityPackage;

/**
 * MyApplication
 *
 * @ProjectName: loadrviewlibrary
 * @ClassName: MyApplication
 * @Description:
 * @Author: lr
 * @CreateDate: 2021/7/20 10:27
 * @since 2021-07-20
 */
public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
    }
}
