# loaderviewlibrary

### 项目介绍

- 项目名称：loaderviewlibrary

- 所属系列：openharmony的第三方组件适配移植

- 功能：为 TextView 和 ImageView 提供在显示任何文本或图像之前显示微光（动画加载器）的能力

- 项目移植状态：主功能完成

- 调用差异：无

- 开发版本：sdk6，DevEco Studio 2.2 Beta1

- 基线版本：Release 3.0.0


### 效果演示

<img src="gif/loadview.gif"></img>

### 安装教程
1.在项目根目录下的build.gradle文件中，
 ```
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
        }
    }
 }
```
2.在entry模块的build.gradle文件中，
 ```
 dependencies {
 implementation('com.gitee.chinasoft_ohos:loaderviewlibrary:0.0.1-SNAPSHOT')
    ......
 }
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

### 使用说明

1. Loader View for TextView defined in layout XML
    ```xml
    <com.elyeproj.loaderviewlibrary.LoaderTextView
         android:layout_width="match_parent"
         android:layout_height="wrap_content" />
    ```

2. Loader View for ImageView defined in layout XML
    ```xml
    <com.elyeproj.loaderviewlibrary.LoaderImageView
         android:layout_width="100dp"
         android:layout_height="100dp" />
    ```

3. Define the % width of the TextView that shows the loading animation with `width_weight`
    ```xml
    <com.elyeproj.loaderviewlibrary.LoaderTextView
         android:layout_width="match_parent"
         android:layout_height="wrap_content"
         app:width_weight="0.4" />
    ```

4. Define the % height of the TextView that shows the loading animation with `height_weight`
    ```xml
    <com.elyeproj.loaderviewlibrary.LoaderTextView
         android:layout_width="match_parent"
         android:layout_height="wrap_content"
         app:height_weight="0.8" />
    ```

5. Define use gradient of the TextView or ImageView that shows the gradient with `use_gradient`
    ```xml
    <com.elyeproj.loaderviewlibrary.LoaderTextView
         android:layout_width="match_parent"
         android:layout_height="wrap_content"
         app:use_gradient="true" />
    ```

6. Define rectangle round radius using `corner`. The default corner is 0.
    ```xml
    <com.elyeproj.loaderviewlibrary.LoaderTextView
         android:layout_width="match_parent"
         android:layout_height="wrap_content"
         app:corners="16" />
    ```

7. Setting the Text Style as BOLD would darken the loading shimmer

8. Other feature of TextView and ImageView is still applicable.

9. Use a custom shimmer color (note: if set, point 7 will not apply, your color will be used even if the Text Style is BOLD)
    ```xml
    <com.elyeproj.loaderviewlibrary.LoaderTextView
         android:layout_width="match_parent"
         android:layout_height="wrap_content"
         app:custom_color="$color:holo_green_dark" />
    ```

10. Reset and show shimmer (animation loader) again by calling the below API
    ```java
    myLoaderTextView.resetLoader();
    myLoaderImageView.resetLoader();
    ```

### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

### 版本迭代

- 0.0.1-SNAPSHOT

### 版权和许可信息

```
 Licensed under the Apache License, Version 2.0 (the "License"); you may not use this work except in compliance with the License. You may obtain a copy of the License in the LICENSE file, or at:

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

```


